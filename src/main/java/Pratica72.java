
import java.io.*;
import java.util.*;
import utfpr.ct.dainf.if62c.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    private static HashMap<String, Integer> cpv = new HashMap<>();
    public static void main(String[] args) throws IOException{
        Scanner scanner = new Scanner(System.in);
        String nomeArquivo; // = "/home/gabriel/NetBeansProjects/palavras.txt";
        
        System.out.print("Arquivo: ");
        nomeArquivo = scanner.next();
        
        System.out.println(nomeArquivo);
        ContadorPalavras cp;
        cp = new ContadorPalavras(nomeArquivo);
        cpv = cp.getPalavras();
 //       SalvaArquivo sa = new SalvaArquivo("/home/gabriel/NetBeansProjects/palavras.out");
        BufferedWriter salva = new BufferedWriter(new FileWriter("/home/gabriel/NetBeansProjects/palavras.out"));
        Set set = cpv.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            String linha = String.format("%s,%d\n",mentry.getKey(),mentry.getValue());
            salva.write(linha);
    
            System.out.print(linha);
        }
        salva.close();
    }
}
