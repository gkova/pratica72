/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.*;
import java.util.*;

/**
 *
 * @author gabriel
 */
public class ContadorPalavras {
    BufferedReader reader;
    private HashMap<String, Integer> hm = new HashMap<>();

    public ContadorPalavras(String nomeArquivo) throws FileNotFoundException {

        reader = new BufferedReader(new FileReader(nomeArquivo));
    }
 
    /**
     *
     * @return
     * @throws java.io.IOException
     */
    public HashMap<String, Integer> getPalavras() throws IOException{
        String linha;

        while ((linha = reader.readLine()) != null) {
            Scanner scanner = new Scanner(linha.toUpperCase());
            String palavra;
            while(scanner.hasNext()){
                palavra = scanner.next();
                if(hm.containsKey(palavra)){
                    int q = (hm.get(palavra)) + 1;
                    hm.put(palavra, q);
                } else {
                    int q = 1;
                    hm.put(palavra, q);
                }
            }
            
        }
        reader.close();
        return hm;
    }
    
}
